#!/usr/bin/env python3

# import pyfits
import os
import random
import numpy
import math
import time
import calc_exposure_and_framerate as calc
from atcore import *

verbose = False    # Prints all tests with a "Requested/Calcualted/Actual" formatted string

# TESTS (Shutter Mode, Acquire)
SHUTTER_MODE=0
ENABLE_ACQUISITION=1
tests=[['Global',False], ['Global',True], ['Rolling',False], ['Rolling',True]]


def Start_Acquisition(sdk3, hndl):
    if verbose: print("Starting acquisition...")
    imageSizeBytes = sdk3.get_int(hndl,"ImageSizeBytes");
    sdk3.queue_buffer(hndl, numpy.empty((imageSizeBytes,), dtype='B').ctypes.data,imageSizeBytes)
    sdk3.command(hndl, "AcquisitionStart")
    if not sdk3.get_bool(hndl, "CameraAcquiring") == 1:
        print("ERROR: Camera is not acquiring!")

def Stop_Acquisition(sdk3, hndl):
    if verbose: print("Stopping acquisition...")
    sdk3.command(hndl, "AcquisitionStop")
    sdk3.flush(hndl)

def Do_Test(sdk3, hndl, testConfig):
    expTimeMin = sdk3.get_float_min(hndl, "ExposureTime")
    expTimeMax = sdk3.get_float_max(hndl, "ExposureTime")
    rowReadTime = sdk3.get_float(hndl, "RowReadTime")
    cameraAcquiring = sdk3.get_bool(hndl, "CameraAcquiring")

    range_min = [expTimeMin,       0.1,   1.0,  10.0, 30]
    range_max = [0.1,              1.0,   10.0, 30.0, expTimeMax]
    range_inc = [0.25*rowReadTime, 0.001, 0.01, 0.1,  1.0]
    idx = 0

    # Setup the calculator
    calc.set_Values( sdk3.get_enumerated_string(hndl, "ElectronicShutteringMode"),\
                     sdk3.get_int(hndl, "AOIHeight"),\
                     sdk3.get_enumerated_string(hndl, "TriggerMode"),\
                     False, 0,\
                     sdk3.get_int(hndl, "AOITop"),\
                     sdk3.get_int(hndl, "AOIVBin"))

    success = True
    longExposureTransition = sdk3.get_float(hndl, "LongExposureTransition")
    currExpTime = sdk3.get_float(hndl, "ExposureTime")
    print()
    for i in range(0, len(range_min)):
        print("Test Range {}: {:11.06f} <= expTime < {:11.06f}, increment: {:.12f}".format(\
            i+1, range_min[i], range_max[i], range_inc[i]))

        for newExpTime in numpy.arange(range_min[i], range_max[i], range_inc[i]):
            stopAcqToChangeExpTime = False
            if testConfig[SHUTTER_MODE] == 'Global' and cameraAcquiring and\
               ((currExpTime < longExposureTransition and\
                 newExpTime >= (longExposureTransition - rowReadTime)) or\
                (currExpTime >= longExposureTransition and\
                 newExpTime < longExposureTransition)):
               stopAcqToChangeExpTime = True
               Stop_Acquisition(sdk3, hndl)

            sdk3.set_float(hndl, "ExposureTime", newExpTime)

            if stopAcqToChangeExpTime:
                Start_Acquisition(sdk3, hndl)

            actualExpTime = sdk3.get_float(hndl, "ExposureTime")
            calcExpTime = calc.calc_exposure(newExpTime)
            error = False
            if abs(actualExpTime-calcExpTime) > 0.0000001:
              print("Requested: {:.06f}, Calculated: {:.17f}, Actual: {:.17f} - ERROR".format(\
                newExpTime, calcExpTime, actualExpTime))
              error = True
              success = False
            elif verbose:
              print("Requested: {:.06f}, Calculated: {:.06f}, Actual: {:.06f}".format(\
                newExpTime, calcExpTime, actualExpTime))

            currExpTime = actualExpTime

    return success

def Camera_On(testConfig):
    print("Intialising SDK3")
    sdk3 = ATCore() # Initialise SDK3
    deviceCount = sdk3.get_int(sdk3.AT_HNDL_SYSTEM,"DeviceCount")
    print("Found : {} devices()".format(deviceCount))
    if deviceCount > 0 :
        try :
            print("Opening camera...");
            hndl = sdk3.open(0);
        except ATCoreException as err :
        	print("     SDK3 Error {0}".format(err));
        	print("  Closing camera");
        	sdk3.close(hndl);
    #sdk3.close(hndl)
    cyclemode='Fixed'
    #cyclemode='Continuous'
    framecount=1
    gainmode='Standard (16-bit)'
    triggermode='External'
    print('Test Settings: ')
    print('    Shutter:', testConfig[SHUTTER_MODE])
    print('  CycleMode:', cyclemode)
    print(' FrameCount:', framecount)
    print('   GainMode:', gainmode)
    print('TriggerMode:', triggermode)
    print('    Acquire:', testConfig[ENABLE_ACQUISITION])

    print('Setting Modes..')
    sdk3.set_int(hndl, "FrameCount", framecount)
    sdk3.set_enumerated_string(hndl, "TriggerMode", triggermode)
    sdk3.set_enumerated_string(hndl, "CycleMode", cyclemode)
    sdk3.set_enumerated_string(hndl, "ElectronicShutteringMode", testConfig[SHUTTER_MODE])
    sdk3.set_enumerated_string(hndl, "SimplePreAmpGainControl", gainmode)
    print('Min Exp: {:.06f}, Max Exp: {:.06f}'.format(sdk3.get_float_min(hndl, "ExposureTime"),\
                                                      sdk3.get_float_max(hndl, "ExposureTime")))
    print('AOILEFT MIN AND MAX: ', sdk3.get_int_min(hndl, "AOILeft"), sdk3.get_int_max(hndl, "AOILeft"))

    if testConfig[ENABLE_ACQUISITION]:
        Start_Acquisition(sdk3, hndl)

    return sdk3, hndl

def Camera_Off(sdk3, hndl):
    #Reinitiate this once testing is completed
    #sdk3.set_bool(hndl, "SensorCooling", 0)
    if sdk3.get_bool(hndl, "CameraAcquiring") == 1:
        Stop_Acquisition(sdk3, hndl)

    time.sleep(1)
    sdk3.close(hndl)
    closed=1
    print ('Camera is now switched off!!')
    return closed

overallSuccess = True
for t in range(0, len(tests)):
    print("\nTEST #{} - Shutter Mode: {}, Acquiring: {}".format(
        t+1, tests[t][SHUTTER_MODE], tests[t][ENABLE_ACQUISITION]))
    sdk3, hndl = Camera_On(tests[t])
    if not Do_Test(sdk3, hndl, tests[t]):
        overallSuccess = False
    Camera_Off(sdk3, hndl)
    print("TEST #{} Complete, Success: {}".format(t+1,overallSuccess))

if overallSuccess:
    print("\nOverall Status: SUCCESS")
else:
    print("\nOverall Status: FAILURE")
