# README #

### What is this repository for? ###

* This repo contains python based Andor Test code. All attempts have been made to make the code portable between the Andor Balor and Andor Zyla cameras. 
* If the test is camera specific, it will be indicated in the discussino below.
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Code in this repository requires the Foreign Function Interface for C (cffi) and the Andor Python SDK3 Wrapper. The SDK3 wrapper may be obtained from Andor by request.
* All tests require Python 3.6,8 or later.
* To install cffi: sudo python3 -m pip install cffi
* To install Andor SDK3 Python Wrapper: sudo python3 -m pip install /path/to/file/pyAndorSDK3
* Tests may be run by: python3 <NameOfTest.py> -or- <NameOfTest.py>

# Test Pograms #
## Test_Calc-vs-Actual_ExpTime (Andor Balor Only) ##
Tests the calculated vs actual exposure time on the Balor.
This test program will iterate through test scenarios with a set of desired exposure times as follows:

| Test #  | Minimum Exposure Time | Maximum Exposure Time | Exposure Time Increment |
| ------- | --------------------- | --------------------- | ----------------------- |
| 1       | Shutter Mode Min      |    0.1s               | (rowReadTime * 0.25)    |
| 2       | 0.1s                  |    1.0s               | 0.001s                  |
| 3       | 1.0s                  |    10.0s              | 0.01s                   |
| 4       | 10.0s                 |    30.0s              | 0.1s                    |
| 5       | 30.0s                 | Shutter Mode Max      | 1.0s                    |

The above 5 exposure time ranges and accompanying increments are repeated for each of the following ElectronicShutterMode/AcquisitionStart combinations:

| ElectronicShutteringMode | AcquistionStart |
| ------------------------ | --------------- |
| Global                   | False           |
| Global                   | True            |
| Rolling                  | False           |
| Rolling                  | True            |

Testing will proceed as follows:

* For each desired exposure time, the Balor calc_exposure_and_framerate.py is called to get a calculated exposure time. 
* The desired exposure time is set on the camera. If the test indicates True for AcquisitionStart, then Acquisition will be stopped when the test exposure time crosses the LongExposureTransition, the exposure time set, and acquisition restarted.
* The actual exposure time is read back and compared to the calculated. 
* The TriggerMode=External to prevent any actual data acquisition. No exposures are actually initiated. 

**Dependencies:** calc_exposure_and_framerate.py (Contained in separate git repo in this project).

### Who do I talk to? ###

* Author and project admin: cberst@nso.edu
* DKIST BitBucket admin: jhubbard@nso.edu